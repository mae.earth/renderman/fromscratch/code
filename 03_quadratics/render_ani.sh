#!/bin/sh
mkdir -p frames
rm ./frames/*.exr ./frames/*.png > /dev/null
go run quadratics_ani.go -scale 0.5 -shape "cone" > quadratics_ani.rib
time render -progress quadratics_ani.rib

